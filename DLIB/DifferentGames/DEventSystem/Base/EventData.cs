﻿using UnityEngine;
using System.Collections;

/**
 * Carry data form dispatcher to listener
*/
namespace DLIB
{
	[System.Serializable]
	public class EventData
	{
		[Header("Event settings")]
		public string eventName = "";
		public Transform[] targets = null;
		[Header("Event carry data")]
		public int intValue = 0;
		public long longValue = 0L;
		public float floatValue = 0f;
		public double doubleValue = 0d;
		public string strValue = "";
		public bool boolValue = false;
		public Vector2 vec2Value = Vector2.zero;
		public Vector3 vec3Value = Vector3.zero;
		public Vector4 vec4Value = Vector4.zero;
		public Bounds boundsValue = new Bounds();
		public Object objValue = null;
		public Object[] objArrayValue = null;
		public Color colorValue = Color.black;
	}
}
