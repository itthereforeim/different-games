﻿using System;

namespace DLIB
{
	internal static class ValueTable
	{
		#region MSG_ message
		public const string MSG_ADD_LISTENER_FAIL = "You can't add an EventListener with null instance or empty EventName.";
		public const string MSG_REMOVE_LISTENER_FAIL = "You can't remove an EventListenr with null instance or empty EventName.";
		public const string MSG_DISPATCH_FAIL = "You can't dispatch an event with null dispatcher instance or empty EventData.";
		#endregion
	}
}

