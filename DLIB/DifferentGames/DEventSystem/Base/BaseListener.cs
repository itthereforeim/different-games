﻿using UnityEngine;
using System.Collections;

/**
 * Base listener
*/
namespace DLIB
{
	//Remove self from AddComponent button list, and act MonoBehaviour callback on Editor step
	[AddComponentMenu(""), ExecuteInEditMode]
	public class BaseListener : MonoBehaviour
	{
		public string EventName = "";

		protected void OnEnable()
		{
			EventCenter.Instance.AddEventListener (this);
			OnEnableEvent ();
		}

		protected void OnDisable()
		{
			EventCenter.Instance.RemoveEventListener (this);
			OnDisableEvent ();
		}

		public virtual void OnEnableEvent()
		{
		}

		public virtual void OnDisableEvent()
		{
		}

		public virtual void OnReceive (BaseDispatcher dispatcher)
		{
		}

		public virtual void OnEvent(BaseDispatcher dispatcher)
		{
		}

		public virtual void OnEventEnd (BaseDispatcher dispatcher)
		{
		}

		[ContextMenu("Receive")]
		public virtual void Receive()
		{
			OnReceive (null);
			OnEvent (null);
			OnEventEnd (null);
		}
	}
}
