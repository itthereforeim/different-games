﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Base dispatcher
*/
namespace DLIB
{
	//Remove self from AddComponent button list
	[AddComponentMenu("")]
	public class BaseDispatcher : MonoBehaviour
	{
		public EventData[] eventData;

		public virtual void Reset()
		{
			eventData = null;
		}

		public virtual void BeforeDispatch(BaseListener targerListener)
		{
		}

		public virtual void OnDispatch(BaseListener targerListener)
		{
		}

		public virtual void AfterDispatch(BaseListener targerListener)
		{
		}
			
		[ContextMenu("Dispatch")]
		public virtual void Dispatch()
		{
			EventCenter.Instance.DispatchEvent (this);
		}
	}
}
