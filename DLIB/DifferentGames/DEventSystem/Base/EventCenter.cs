﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Event center
 * 
 * [Simple flow chart]
 * Dispatcher.Dispatch() > EventCenter.DispatchEvent() > Dispatcher.BeforeDispatch() > Dispatcher.OnDispatch() >
 * Listener.OnReceive() > Listener.OnEvent() > Listener.OnEventEnd()
*/
namespace DLIB
{
	public class EventCenter
	{
		#region Singleton
		private static EventCenter mInstance;
		public static EventCenter Instance
		{
			get
			{
				if(mInstance == null)
					mInstance = new EventCenter ();
				return mInstance;
			}
		}
		#endregion

		//Current listener list dictionary
		private Dictionary<string, List<BaseListener>> eventListeners = new Dictionary<string, List<BaseListener>>();

		#region Public functions
		/**
		 * Add event listener
		*/
		public void AddEventListener(BaseListener listener)
		{
			//Check input
			if ( listener != null && !string.IsNullOrEmpty (listener.EventName) )
			{
				//Get listener list
				List<BaseListener> listenerList = eventListeners [listener.EventName];

				//Create listener list if needed
				if ( listenerList == null )
				{
					eventListeners.Add (listener.EventName, new List<BaseListener> ());
					listenerList = eventListeners [listener.EventName];
				}

				//Add input listener if not contains that one
				if ( listenerList.Contains (listener) == false )
				{
					listenerList.Add (listener);
				}
			}
			else
			{
				Debug.LogError (ValueTable.MSG_ADD_LISTENER_FAIL);
			}
		}

		/**
		 * Remove event listener
		*/
		public void RemoveEventListener(BaseListener listener)
		{
			//Check input
			if ( listener != null && !string.IsNullOrEmpty (listener.EventName) )
			{
				string key = listener.EventName;

				//Remove listener form list
				List<BaseListener> listenerList = eventListeners [key];

				//Remove listener list from dictionary, if count <= 0
				if ( listenerList != null )
				{
					listenerList.Remove (listener);
					if ( listenerList.Count <= 0 )
					{
						eventListeners.Remove (key);
					}
				}
			}
			else
			{
				Debug.LogError (ValueTable.MSG_REMOVE_LISTENER_FAIL);
			}
		}

		/**
		 * Dispatch event
		*/
		public void DispatchEvent(BaseDispatcher dispatcher)
		{
			//Check input
			if ( dispatcher != null && dispatcher.eventData != null )
			{
				//Dispatch each EventData
				for ( var i = 0; i < dispatcher.eventData.Length; i++ )
				{
					EventData eventData = dispatcher.eventData [i];
					List<BaseListener> listenerList = eventListeners [eventData.eventName];

					if ( listenerList != null)
					{
						for ( var j = 0; j < listenerList.Count; j++ )
						{
							BaseListener listener = listenerList [j];
							if(listener != null)
							{
								//Send to targets only
								if ( eventData.targets != null && eventData.targets.Length > 0 )
								{
									for ( var k = 0; k < eventData.targets.Length; k++ )
									{
										Transform target = eventData.targets [k];
										if ( listener.transform == target )
											RunDispatchEventFlow (dispatcher, listener);
									}
								}
								//Send to all
								else
								{
									RunDispatchEventFlow (dispatcher, listener);
								}
							}
						}
					}
				}
			}
			else
			{
				Debug.LogError (ValueTable.MSG_DISPATCH_FAIL);
			}
		}

		private void RunDispatchEventFlow(BaseDispatcher dispatcher, BaseListener listener)
		{
			//BeforeDispatch callback
			dispatcher.BeforeDispatch (listener);
			//OnDispatch callback
			dispatcher.OnDispatch (listener);
			listener.OnReceive (dispatcher);
			//AfterDispatch callback
			dispatcher.AfterDispatch (listener);
			listener.OnEvent (dispatcher);
			listener.OnEventEnd (dispatcher);
		}
		#endregion
	}
}
