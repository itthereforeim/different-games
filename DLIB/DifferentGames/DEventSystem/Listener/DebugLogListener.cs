﻿using UnityEngine;
using System.Collections;

namespace DLIB
{
	public class DebugLogListener : BaseListener
	{
		public enum LogType
		{
			Log,
			Warning,
			Error,
			MessageBox
		}

		public LogType logType = LogType.Log;
		public string message = "";

		public override void OnEvent (BaseDispatcher dispatcher)
		{
			switch(logType)
			{
				case LogType.Log:
					Debug.Log (message);
					break;
				case LogType.Warning:
					Debug.LogWarning (message);
					break;
				case LogType.Error:
					Debug.LogError (message);
					break;
				case LogType.MessageBox:
					#if UNITY_EDITOR
					UnityEditor.EditorUtility.DisplayDialog("DebugLogListener", message, "OK");
					#endif
					break;
				default:
					Debug.Log (message);
					break;
			}
		}
	}
}
