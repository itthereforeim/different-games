﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace DLIB
{
	[CustomEditor(typeof(BaseListener), true)]
	public class BaseListenerEditor : Editor
	{
		BaseListener listener;

		void OnEnable()
		{
			listener = target as BaseListener;
		}

		public override void OnInspectorGUI ()
		{
		}
	}
}
